var stats, scene, renderer;
var camera, cameraControls;
var bFullScreenInfoAdded = false;

var numLines = 20;
var interval_i = Math.PI*2/numLines;

var radiusFactor = 50;

var vaseThickness = 10;

var hasStat =false;

var dataSampleRate = 60;
var interval_y = 3;

var bottomThickness = 2;

var processedData;

var originalW;
var originalH;

var verticesStr = "";
var faceStr = "";
var verticesCount = 0;
var faceCnt = 0;

var meshHeight;
var meshY;

var imageData;
var mode = 0;
var data = [];
const NORMAL = 0;
const RECORD = 1;

function save()
{
	if (verticesStr && faceStr)
	{
		document.getElementById('bottomInfo').innerHTML+="<a id='test' href='data:text;charset=utf-8,"+encodeURIComponent(verticesStr + faceStr)+"' download= 'pottery.obj'>download .OBJ file</a>";
	}
}

function drawMiddleLine(ctx)
{
	ctx.beginPath();
    ctx.moveTo( ctx.canvas.width / 2, 0 );
	ctx.lineTo( ctx.canvas.width  / 2, ctx.canvas.height - 3);
	ctx.lineWidth = 1;
	ctx.strokeStyle = 'white';
	ctx.stroke();
}

function drawData(ctx, data , length, resizeFactor)
{
		ctx.beginPath();
        ctx.moveTo( ctx.canvas.width / 2 - (originalW / 2 - data[0][0]) * resizeFactor,ctx.canvas.height / 2 - (originalH / 2 - data[0][1]) * resizeFactor);
        for (var i = 1; i < length; i++) {
            ctx.lineTo(  ctx.canvas.width  / 2 - (originalW / 2 - data[i][0]) * resizeFactor, ctx.canvas.height / 2 - (originalH / 2 - data[i][1]) * resizeFactor);
        }
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
        
        ctx.beginPath();
        ctx.moveTo( ctx.canvas.width  / 2 + (originalW / 2 - data[0][0]) * resizeFactor, ctx.canvas.height / 2 - (originalH / 2 - data[0][1]) * resizeFactor);
        for(var i = 1; i < length; i++){
          ctx.lineTo( ctx.canvas.width  / 2 + (originalW / 2 - data[i][0]) * resizeFactor, ctx.canvas.height / 2 - (originalH / 2 - data[i][1]) * resizeFactor);
        }
        ctx.lineWidth =1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
}

function reverseData(data)
{
	for (var i = 0; i< data.length/2; i++)
	{
		var temp = data[i].slice();
		data[i] = data[data.length - 1 - i];
		data[data.length - 1 - i] = temp;
	}
}

function executeThreeJS( data ){

  	var c = document.getElementById("inputCanvas");

	var ctx;
  	ctx = c.getContext("2d");

	originalW = ctx.canvas.width;
  	originalH = ctx.canvas.height;
  	
	if( !init() )	animate();

	// init the scene
	function init(){

		if( Detector.webgl ){
			renderer = new THREE.WebGLRenderer({
				antialias		: true,	// to get smoother output
				preserveDrawingBuffer	: true,	// to allow screenshot
				canvas : document.getElementById('mycanvas')
			});
			//renderer.setClearColorHex( 0xBBBBBB, 1 );
			renderer.setClearColor(0xBBBBBB, 1);
		// uncomment if webgl is required
		//}else{
		//	Detector.addGetWebGLMessage();
		//	return true;
		}else{
			renderer	= new THREE.CanvasRenderer();
		}
		//renderer.setSize(document.getElementById('mycanvas').offsetWidth, document.getElementById('mycanvas').offsetHeight);
		if (!document.getElementById('outputContainer').hasChildNodes())
		{
			document.getElementById('outputContainer').appendChild(renderer.domElement);					
		}


		// add Stats.js - https://github.com/mrdoob/stats.js
		if (!hasStat)
		{
			stats = new Stats();
			stats.domElement.style.position	= 'absolute';
			stats.domElement.style.bottom	= '0px';
			document.body.appendChild( stats.domElement );	
			hasStat =true;				
		}


		// create a scene
		scene = new THREE.Scene();

		// put a camera in the scene
		camera = new THREE.PerspectiveCamera(60, (document.getElementById('outputContainer').offsetWidth - 3)/ (document.getElementById('outputContainer').offsetHeight - 3), 1, 10000 );
		camera.position.set(0, 0, 10);
		scene.add(camera);

		// create a camera contol
		cameraControls	= new THREEx.DragPanControls(camera, renderer.domElement);

		// transparently support window resize
		//THREEx.WindowResize.bind(renderer, camera);
		// allow 'p' to make screenshot
		THREEx.Screenshot.bindKey(renderer);
		

		function getPointByRadiusAngleAndY(inRadius, inAngle, inY)
		{
			var x = Math.sin(inAngle) * inRadius;
			var z = Math.cos(inAngle) * inRadius;

			var point = new THREE.Vector3(x,meshY-inY,z);

			return point;
		}

		function getFaceByVerticesIndices(inVI1,inVI2,inVI3)
		{
			var v1 = geometry.vertices[inVI1];
			var v2 = geometry.vertices[inVI2];
			var v3 = geometry.vertices[inVI3];

			var face = new THREE.Face3(inVI1, inVI2, inVI3);
			var U1 = new THREE.Vector3();
			var U2 = new THREE.Vector3();
			U1.subVectors(v3, v2);
			U2.subVectors(v1, v2);
			var normal1 = new THREE.Vector3();
			normal1.crossVectors(U1,U2);
			face.normal = normal1;

			return face;			
		}

		function getPointOnSpline( p0, p1, p2, p3, t ) {
		var result = new Array(2);
		result[0] = 0.5*((2*p1[0])+
				(-p0[0]+p2[0])*t+
				(2*p0[0]-5*p1[0]+4*p2[0]-p3[0])*t*t+
				(-p0[0]+3*p1[0]-3*p2[0]+p3[0])*t*t*t);
		result[1] = 0.5*((2*p1[1])+
				(-p0[1]+p2[1])*t+
				(2*p0[1]-5*p1[1]+4*p2[1]-p3[1])*t*t+
				(-p0[1]+3*p1[1]-3*p2[1]+p3[1])*t*t*t);
		return result;
		}

		//var vertexIndex = 0;

		var geometry = new THREE.Geometry();
		//nested loop to compute polygons
		//i is the counter revolve around y-axis 360 degrees


		//Process the raw data to produce smooth line;

		processedData = new Array();

		var pickedData = new Array();

		var dataCnt = 0;
		var yCnt = 0;

		pickedData[dataCnt] = data[0].slice();
		dataCnt++;

		var currenti = 0;

		while (dataCnt == 1)
		{
			currenti++;
			if (Math.abs(data[currenti][0] - pickedData[0][0])+Math.abs(data[currenti][1]-pickedData[0][1]) >= dataSampleRate / 5)
			{
				pickedData[dataCnt] = data[currenti].slice();
				dataCnt++;
			}
		}
		for (var i = currenti ; i < data.length - 1 ; i++)
		{
			if (Math.abs(data[i][0] - pickedData[dataCnt-1][0])+Math.abs(data[i][1]-pickedData[dataCnt-1][1]) >= dataSampleRate)
			{
				pickedData[dataCnt] = data[i].slice();
				dataCnt++;
			}
		}

		pickedData[dataCnt] = data[data.length - 1].slice();
		dataCnt++;


		for (var i = 1; i < pickedData.length - 2 ; i ++)
		{
			processedData[yCnt] = pickedData[i].slice();
			yCnt++;
			for (var j = 1 ; j < interval_y + 1 ; j++)
			{
				processedData[yCnt] = getPointOnSpline(pickedData[i - 1], 
														pickedData[i], 
														pickedData[i + 1], 
														pickedData[i+ 2], 
														j / (interval_y + 1));
				yCnt++;						
			}

		}

		processedData[yCnt] = pickedData[pickedData.length - 2];
		yCnt++;

		meshHeight = (processedData[processedData.length-1][1] - processedData[0][1])/radiusFactor;

		if (meshHeight<0)
		{
			meshY = processedData[0][1]/radiusFactor;
			meshHeight = - meshHeight;
			
			reverseData(processedData);
		}
		else
		{
			meshY = processedData[processedData.length-1][1]/radiusFactor;	
		}

		//redraw the processed data on input canvas
  		var c = document.getElementById("inputCanvas");

  		ctx = c.getContext("2d");
  		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

  		drawMiddleLine(ctx);



  		drawData(ctx, processedData, processedData.length, 1);

		var numVerticesPerLine = processedData.length;

		var numVerticesPerInnerLine = numVerticesPerLine - bottomThickness;

		//initialization: pushing the first line of vertices into vertex array
		for (var y = numVerticesPerLine - 1; y >= 0 ; y = y - 1)
		{
			var radius = ctx.canvas.width/2 - processedData[y][0];
	      	radius /=radiusFactor;

	        var point = getPointByRadiusAngleAndY(radius,Math.PI*2,processedData[y][1]/radiusFactor);

	       	geometry.vertices.push(point);
	       	verticesCount++;

			verticesStr += "v " + point.x + " " + point.y + " " + point.z + "\n";
		}

		//pushing faces into face array, start with pushing the current line of vertices into the vertex array

		for (var i = Math.PI * 2 - interval_i; i > 0; i = i - interval_i) 
		{

			//pushing the first point in the current line into vertex array, both in and out.

			var radius = ctx.canvas.width/2 -  processedData[numVerticesPerLine - 1][0];

			radius /= radiusFactor;

			var point = getPointByRadiusAngleAndY(radius,i,processedData[numVerticesPerLine - 1][1]/radiusFactor);

	       	geometry.vertices.push(point);
	       	verticesCount++;
	       	verticesStr += "v " + point.x + " " + point.y + " " + point.z + "\n";
			//y is the counter for each point on the vertical curve

			//pushing vertexes along the line and push faces at the same time
		    for(var y = numVerticesPerLine - 2; y >= 0 ; y = y - 1)
		    {			    	
		    	
				var radius = ctx.canvas.width/2 -  processedData[y][0] ;

				radius /=radiusFactor;

				var point = getPointByRadiusAngleAndY(radius,i,processedData[y][1]/radiusFactor);
		       	geometry.vertices.push(point);
		       	verticesCount++;
				
				verticesStr += "v " + point.x + " " + point.y + " " + point.z + "\n";

					var index1 = verticesCount - numVerticesPerLine - 2;
					var index2 = verticesCount - 2;
					var index3 = verticesCount - 1;
					var v1 = geometry.vertices[index1];
					var v2 = geometry.vertices[index2];
					var v3 = geometry.vertices[index3];

					var face = getFaceByVerticesIndices(index3, index2, index1);

					geometry.faces.push(face);
					faceCnt++;

					faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";

					var index1 = verticesCount - numVerticesPerLine - 2;
					var index2 = verticesCount - 1;
					var index3 = verticesCount - numVerticesPerLine - 1;
					var v1 = geometry.vertices[index1];
					var v2 = geometry.vertices[index2];
					var v3 = geometry.vertices[index3];

					var face = getFaceByVerticesIndices(index3, index2, index1);

					geometry.faces.push(face);
					faceCnt++;

					faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";

			}
		}

		//draw the last stripe of the vase

		for(var i = 1; i < numVerticesPerLine ; i++)
		    {		
	    	
					var index1 = verticesCount - numVerticesPerLine + i - 1;
					var index2 = i - 1;
					var index3 = i;
					var v1 = geometry.vertices[index1];
					var v2 = geometry.vertices[index2];
					var v3 = geometry.vertices[index3];

					var face = getFaceByVerticesIndices(index3, index2, index1);

					geometry.faces.push(face);
					faceCnt++;

					faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";

					var index1 = verticesCount - numVerticesPerLine + i - 1;
					var index2 = i;
					var index3 = verticesCount - numVerticesPerLine + i;
					var v1 = geometry.vertices[index1];
					var v2 = geometry.vertices[index2];
					var v3 = geometry.vertices[index3];

					var face = getFaceByVerticesIndices(index3, index2, index1);

					geometry.faces.push(face);
					faceCnt++;

					faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";

			}

		// draw the inner side of the vase
		//initialization: pushing the first line of vertices into vertex array
		for (var y = numVerticesPerInnerLine - 1; y >= 0 ; y = y - 1)
		{
			var radius = ctx.canvas.width/2 - processedData[y][0] - vaseThickness;

	      	radius /=radiusFactor;

	        var point = getPointByRadiusAngleAndY(radius,Math.PI*2,processedData[y][1]/radiusFactor);

	       	geometry.vertices.push(point);
	       	verticesCount++;
	       	verticesStr += "v " + point.x + " " + point.y + " " + point.z + "\n";
		}

		//pushing faces into face array, start with pushing the current line of vertices into the vertex array

		for (var i = Math.PI * 2 - interval_i; i > 0; i = i - interval_i) 
		{


			var radius = ctx.canvas.width/2 -  processedData[numVerticesPerInnerLine - 1][0] - vaseThickness;
			radius /= radiusFactor;

			var point = getPointByRadiusAngleAndY(radius,i,processedData[numVerticesPerInnerLine - 1][1]/radiusFactor);

	       	geometry.vertices.push(point);
	       	verticesCount++;
	       	verticesStr += "v " + point.x + " " + point.y + " " + point.z + "\n";

			//pushing vertexes along the line and push faces at the same time
		    for(var y = numVerticesPerInnerLine - 2; y >= 0 ; y = y - 1)
		    {			    	
		    	
				var radius = ctx.canvas.width/2 -  processedData[y][0] - vaseThickness;

				radius /=radiusFactor;

				var point = getPointByRadiusAngleAndY(radius,i,processedData[y][1]/radiusFactor);

		       	geometry.vertices.push(point);
		       	verticesCount++;
		       	verticesStr += "v " + point.x + " " + point.y + " " + point.z + "\n";

				//Draw the faces with vertexIndex verticesCount - numVerticesPerLine -2, verticesCount -1, verticesCount - 2, verticesCount -numVerticesPerLine -1

				//Draw the first face with vertexIndex verticesCount - numVerticesPerLine-2, verticesCount - 2, verticesCount -1

				var index1 = verticesCount - 1;
				var index2 = verticesCount - 2;
				var index3 = verticesCount - numVerticesPerInnerLine - 2;
				var v1 = geometry.vertices[index1];
				var v2 = geometry.vertices[index2];
				var v3 = geometry.vertices[index3];

				var face = getFaceByVerticesIndices(index3, index2, index1);

				geometry.faces.push(face);
				faceCnt++;

				faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";


				//Draw the second face with vertex index verticesCount - numVerticesPerLine - 2, verticesCount -1, verticesCount - numVerticesPerLine - 1

				var index1 = verticesCount - numVerticesPerInnerLine - 1;
				var index2 = verticesCount - 1;
				var index3 = verticesCount - numVerticesPerInnerLine - 2;
				var v1 = geometry.vertices[index1];
				var v2 = geometry.vertices[index2];
				var v3 = geometry.vertices[index3];

				var face = getFaceByVerticesIndices(index3, index2, index1);

				geometry.faces.push(face);
				faceCnt++;

				faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";

			}
		}

		// draw the last stripe of the inner side
		for(var i = 1; i < numVerticesPerInnerLine ; i++)
	    {		
				var index1 = numVerticesPerLine * numLines + i;
				var index2 = numVerticesPerLine * numLines + i - 1;
				var index3 = verticesCount - numVerticesPerInnerLine + i - 1;
				var v1 = geometry.vertices[index1];
				var v2 = geometry.vertices[index2];
				var v3 = geometry.vertices[index3];

				var face = getFaceByVerticesIndices(index3, index2, index1);

				geometry.faces.push(face);
				faceCnt++;

				faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";

				var index1 = verticesCount - numVerticesPerInnerLine + i;
				var index2 = numVerticesPerLine * numLines +i;
				var index3 = verticesCount - numVerticesPerInnerLine + i - 1;
				var v1 = geometry.vertices[index1];
				var v2 = geometry.vertices[index2];
				var v3 = geometry.vertices[index3];

				var face = getFaceByVerticesIndices(index3, index2, index1);

				geometry.faces.push(face);
				faceCnt++;

				faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";
		}
		
      	//Make the bottom so it becomes a vase
			var radius = (ctx.canvas.width/2 - processedData[numVerticesPerLine - 1][0])/radiusFactor;

			var center = new THREE.Vector3(0,meshY-processedData[numVerticesPerLine - 1][1]/radiusFactor,0);
			geometry.vertices.push(center); // Index: numVerticesPerLine * numLines + numVerticesPerInnerLine * numLines
			verticesCount ++ ;
			verticesStr += "v " + center.x + " " + center.y + " " + center.z + "\n";

			for (var i = 1;i< numLines;i++)
			{
				var index1 = (i-1)*numVerticesPerLine;
				var index2 = verticesCount -1; 
				var index3 = i * numVerticesPerLine;
				var v1 = geometry.vertices[index1];
				var v2 = geometry.vertices[index2];
				var v3 = geometry.vertices[index3];

				var face = getFaceByVerticesIndices(index3, index2, index1);

				geometry.faces.push(face);
				faceCnt++;

				faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";					
			}

				var index1 = (numLines-1) * numVerticesPerLine;
				var index2 = verticesCount -1; 
				var index3 = 0;
				var v1 = geometry.vertices[index1];
				var v2 = geometry.vertices[index2];
				var v3 = geometry.vertices[index3];

				var face = getFaceByVerticesIndices(index3, index2, index1);

				geometry.faces.push(face);
				faceCnt++;

				faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";

		//make the inner Bottom
			var radius = (ctx.canvas.width/2 - processedData[numVerticesPerInnerLine - 1][0])/radiusFactor;

			var center = new THREE.Vector3(0,meshY-processedData[numVerticesPerInnerLine - 1][1]/radiusFactor,0);
			geometry.vertices.push(center); // Index: numVerticesPerLine * numLines + numVerticesPerInnerLine * numLines + 1
			verticesCount++;
			verticesStr += "v " + center.x + " " + center.y + " " + center.z + "\n";

			for (var i = 1;i< numLines;i++)
			{
				//vertex index: numLines* numVerticesPerLine + (i- 1)*numVerticesPerInnerLine, numLines * numVerticesPerLine + i * numVerticesPerInnerLine, verticesCount -1
				var index1 = numLines * numVerticesPerLine + i * numVerticesPerInnerLine;
				var index2 = verticesCount - 1;
				var index3 = numLines* numVerticesPerLine + (i- 1)*numVerticesPerInnerLine;
				var v1 = geometry.vertices[index1];
				var v2 = geometry.vertices[index2];
				var v3 = geometry.vertices[index3];

				var face = getFaceByVerticesIndices(index3, index2, index1);

				geometry.faces.push(face);
				faceCnt++;

				faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";				
			}
				var index1 = numLines * numVerticesPerLine;
				var index2 = verticesCount -1; 
				var index3 = numLines * numVerticesPerLine + (numLines-1) * numVerticesPerInnerLine;
				var v1 = geometry.vertices[index1];
				var v2 = geometry.vertices[index2];
				var v3 = geometry.vertices[index3];

				var face = getFaceByVerticesIndices(index3, index2, index1);

				geometry.faces.push(face);
				faceCnt++;

				faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";	
		
		//make the edge
			for (var i = 1; i < numLines; i++)
			{
				//Draw the first face with vertices: i * numVerticesPerLine - 1, (i + 1) * numVerticesPerLine - 1, numLines * numVerticesPerLine + (i +1) * numVerticesPerInnerLine - 1

					var index1 = i * numVerticesPerLine - 1;
					var index2 = (i + 1) * numVerticesPerLine - 1;
					var index3 = numLines * numVerticesPerLine + (i +1) * numVerticesPerInnerLine - 1
					var v1 = geometry.vertices[index1];
					var v2 = geometry.vertices[index2];
					var v3 = geometry.vertices[index3];

					var face = getFaceByVerticesIndices(index3, index2, index1);

					geometry.faces.push(face);
					faceCnt++;

					faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";


				//Draw the second face with vertices: numLines * numVerticesPerLine + i * numVerticesPerInnerLine - 1, numLines * numVerticesPerLine + (i +1) * numVerticesPerInnerLine - 1 , i * numVerticesPerLine - 1

					var index1 = i * numVerticesPerLine - 1;
					var index2 = numLines * numVerticesPerLine + (i +1) * numVerticesPerInnerLine - 1;
					var index3 = numLines * numVerticesPerLine + i * numVerticesPerInnerLine - 1;
					var v1 = geometry.vertices[index1];
					var v2 = geometry.vertices[index2];
					var v3 = geometry.vertices[index3];

					var face = getFaceByVerticesIndices(index3, index2, index1);

					geometry.faces.push(face);
					faceCnt++;

					faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";

			}

					var index1 = numLines * numVerticesPerLine - 1;
					var index2 = numVerticesPerLine - 1;
					var index3 = numLines * numVerticesPerLine + numVerticesPerInnerLine - 1
					var v1 = geometry.vertices[index1];
					var v2 = geometry.vertices[index2];
					var v3 = geometry.vertices[index3];

					var face = getFaceByVerticesIndices(index3, index2, index1);

					geometry.faces.push(face);
					faceCnt++;

					faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";

					var index1 = numLines * numVerticesPerLine - 1;
					var index2 = numLines * numVerticesPerLine + numVerticesPerInnerLine - 1;
					var index3 = numLines * numVerticesPerLine + numLines * numVerticesPerInnerLine - 1;
					var v1 = geometry.vertices[index1];
					var v2 = geometry.vertices[index2];
					var v3 = geometry.vertices[index3];

					var face = getFaceByVerticesIndices(index3, index2, index1);

					geometry.faces.push(face);
					faceCnt++;

					faceStr += "f " + (index3 + 1) + " " + (index2 + 1) + " " + (index1 + 1) + "\n";					

	    //console.log(verticesStr + faceStr);
	    var material	= new THREE.MeshNormalMaterial({
	    	'shading': THREE.PhongShading,
	    	'wireframe':false
	    });
		var mesh	= new THREE.Mesh( geometry, material);
		// mesh.rotation.x = 360;
		 mesh.position.y = - meshHeight/2;
		 //mesh.rotation.x = Math.PI;

		 //mesh.geometry.computeBoundingBox();
		 //var box = mesh.geometry.boundingBox.clone();
		 // var Vmin = new THREE.Vector3(box.min.x, box.min.y, box.min.z);
		 // var Vmax = new THREE.Vector3(box.max.x, box.max.y, box.max.z);
		 //var boxGeo = new THREE.CubeGeometry(box.max.x - box.min.x, box.max.y-box.min.y, box.max.z-box.min.z);
		 //var wireframeMaterial = new THREE.MeshBasicMaterial( { color: 0x00ee00, wireframe: true, transparent: true } ); 
		 //var boxMesh = new THREE.Mesh( boxGeo, wireframeMaterial);
		scene.add( mesh );
		//scene.add( boxMesh );

		save();
		
	}

	// animation loop
	function animate() {

		// loop on request animation loop
		// - it has to be at the begining of the function
		// - see details at http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
		requestAnimationFrame( animate );

		// do the render
		render();

		// update stats
		stats.update();
	}

	// render the scene
	function render() {

		// update camera controls
		cameraControls.update();

		// actually render the scene
		renderer.render( scene, camera );
	}

}

// this is the script for interfacing with Leap and get Input Data to Three.js

window.onload = function()
{
	var e = document.getElementById('mycanvas');
	e.width =document.getElementById("outputContainer").offsetWidth - 3;
	e.height = document.getElementById("outputContainer").offsetHeight - 3;

  	var c = document.getElementById("inputCanvas");

	var ctx;
  	ctx = c.getContext("2d");
  	ctx.canvas.width = document.getElementById("inputContainer").offsetWidth;
  	ctx.canvas.height = document.getElementById("inputContainer").offsetHeight;
  	ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  	imageData = ctx.createImageData(ctx.canvas.width, ctx.canvas.height);

  	drawMiddleLine(ctx);

  	regEvent();

		// allow 'f' to go fullscreen where this feature is supported
	if( THREEx.FullScreen.available() && !bFullScreenInfoAdded){
		THREEx.FullScreen.bindKey();
		bFullScreenInfoAdded = true;		
		document.getElementById('bottomInfo').innerHTML	+= "- <i>f</i> &nbsp; for fullscreen &nbsp;";
	}
  // c.parentNode.removeChild(c);
  // executeThreeJS();
}

window.onresize = function()
{
	if (renderer)
	{
		renderer.setSize(document.getElementById('outputContainer').offsetWidth - 3, document.getElementById('outputContainer').offsetHeight - 3);
		camera.aspect	= (document.getElementById('outputContainer').offsetWidth - 3) / (document.getElementById('outputContainer').offsetHeight - 3);
		camera.updateProjectionMatrix();	
	}

	document.getElementById('mycanvas').width = document.getElementById('outputContainer').offsetWidth - 3;
	document.getElementById('mycanvas').height = document.getElementById('outputContainer').offsetHeight - 3;

	var newW = document.getElementById('inputContainer').offsetWidth;
	var newH = document.getElementById('inputContainer').offsetHeight;
	var resizeFactorX = newW/originalW;
	var resizeFactorY = newH/originalH;

  	var c = document.getElementById("inputCanvas");

	var ctx;
  	ctx = c.getContext("2d");

	ctx.canvas.width = newW;
	ctx.canvas.height = newH;	
  	ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  	drawMiddleLine(ctx);

	if(processedData)
	{
		var resizeFactor;

		resizeFactor = Math.min(resizeFactorX,resizeFactorY);	
		drawData(ctx, processedData, processedData.length, resizeFactor);		
	}



}

function regEvent () {
	var el = document.getElementById("inputCanvas");
	el.addEventListener("touchstart", handleStart, false);
	el.addEventListener("touchend", handleEnd, false);
	el.addEventListener("touchcancel", handleCancel, false);
	el.addEventListener("touchleave", handleEnd, false);
	el.addEventListener("touchmove", handleMove, false);

	el.addEventListener("mousedown", handleStart, false);
	el.addEventListener("mouseup", handleEnd, false);
	el.addEventListener("mousemove", handleMouseMove, false);
}

function handleEnd (evt) {
	evt.preventDefault();
	var el = document.getElementById("inputCanvas");
	el.removeEventListener('touchstart', handleStart, false);
	el.removeEventListener("touchend", handleEnd, false);
	el.removeEventListener("touchcancel", handleCancel, false);
	el.removeEventListener("touchleave", handleEnd, false);
	el.removeEventListener("touchmove", handleMove, false);

	el.removeEventListener("mousedown", handleStart, false);
	el.removeEventListener("mouseup", handleEnd, false);
	el.removeEventListener("mousemove", handleMouseMove, false);
	mode = NORMAL;
	executeThreeJS(data);

	console.log('touchend');
}

function handleMove (evt) {
	evt.preventDefault();
	
	if(mode == RECORD) {
		var c = document.getElementById("inputCanvas");

		var ctx = c.getContext("2d");

  		var tempX = event.touches[0].clientX;
  		console.log(event.touches[0].clientX);
  		if (event.touches[0].clientX > ctx.canvas.width / 2)
  		{
  			tempX = ctx.canvas.width - event.touches[0].clientX;
  		}

  		if (tempX > ctx.canvas.width / 2 - 1.5* vaseThickness)
  		{
  			data.push([ctx.canvas.width / 2 - 1.5* vaseThickness,event.touches[0].clientY])
  		}
		else
		{
			data.push([tempX,event.touches[0].clientY]);
		}
		ctx.beginPath();
        ctx.moveTo( data[0][0], data[0][1] );
        for (var i = 1; i < data.length-2; i++) {
            ctx.lineTo(  data[i][0], data[i][1] );
        }
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
        
        ctx.beginPath();
        ctx.moveTo( ctx.canvas.width - data[0][0], data[0][1] );
        for(var i = 1; i < data.length-2; i++){
          ctx.lineTo( ctx.canvas.width-data[i][0], data[i][1]);
        }
        ctx.lineWidth =1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
	}
}

function handleStart (evt) {
	evt.preventDefault();
	console.log('touchstart');
	mode = RECORD;
	
}

function handleMouseMove (evt) {
	if(mode == RECORD){
		evt.preventDefault();
		var c = document.getElementById("inputCanvas");

		var ctx = c.getContext("2d");

  		var tempX = event.clientX;
  		if (event.clientX > ctx.canvas.width / 2)
  		{
  			tempX = ctx.canvas.width - event.clientX;
  		}

  		if (tempX > ctx.canvas.width / 2 - 1.5* vaseThickness)
  		{
  			data.push([ctx.canvas.width / 2 - 1.5* vaseThickness,event.clientY])
  		}
		else
		{
			data.push([tempX,event.clientY]);
		}
		ctx.beginPath();
        ctx.moveTo( data[0][0], data[0][1] );
        for (var i = 1; i < data.length-2; i++) {
            ctx.lineTo(  data[i][0], data[i][1] );
        }
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
        
        ctx.beginPath();
        ctx.moveTo( ctx.canvas.width - data[0][0], data[0][1] );
        for(var i = 1; i < data.length-2; i++){
          ctx.lineTo( ctx.canvas.width-data[i][0], data[i][1]);
        }
        ctx.lineWidth =1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
	}
}

function handleCancel (evt) {
	evt.preventDefault();
	console.log('touchcancel');
}

document.onkeydown = function() {
   var KeyID = event.keyCode;
  if(KeyID == 76){
    mode = RECORD;
  }
}

 document.onkeyup = function(event) {
  mode = NORMAL;
  if(event.keyCode == 76){
  	executeThreeJS(data);
  }
}

var xmax = 0;
var xmin = Infinity;
var ymax = 0;
var ymin = Infinity;

var last_val = [0,0];

  function moveFinger( posX, posY, posZ, dirX, dirY, dirZ) {

  	var c = document.getElementById("inputCanvas");

	var ctx;
  	ctx = c.getContext("2d");

    var can_x = posX + ctx.canvas.width/2;
    var can_y = ctx.canvas.height - posY;
      if(last_val[0] == can_x && last_val[1] == can_y) return;
       ctx.fillStyle = "#000000";
       ctx.fillRect(can_x,can_y, 1, 1);
      if(mode == RECORD){

        data.push([can_x,can_y]);
        ctx.beginPath();
        ctx.moveTo( data[0][0], data[0][1] );
        for (var i = 1; i < data.length-2; i++) {
            ctx.lineTo(  data[i][0], data[i][1] );
        }
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
        
        ctx.beginPath();
        ctx.moveTo( ctx.canvas.width - data[0][0], data[0][1] );
        for(var i = 1; i < data.length-2; i++){
          ctx.lineTo( ctx.canvas.width-data[i][0], data[i][1]);
        }
        ctx.lineWidth =1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
      }else{
        var default_cl = [0, 0, 0];
      }
      last_val = [ can_x, can_y];
  }

 
  var fingers = {};
  Leap.loop(function(frame) {

    var finger = frame.pointables[0];
    if(!finger) return;
    var posX = finger.tipPosition.x;
    var posY = finger.tipPosition.y;
    var posZ = (finger.tipPosition.y*3) - 400;
    var dirX = -(finger.direction.y*90);
    var dirY = -(finger.direction.z*90);
    var dirZ = (finger.direction.x*90);
    
        
    moveFinger( posX, posY, posZ, dirX, dirY, dirZ);

	//if in state 2 (the model is displayed), rotate and scale using mouse and leap input
	if(cameraControls){
		var event = new CustomEvent('leapmove',{ 'detail':{'posX': posX, 'posY':posY } });
// Dispatch the event.
document.dispatchEvent(event);
	}
  });

